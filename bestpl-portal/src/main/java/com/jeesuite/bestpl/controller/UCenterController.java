package com.jeesuite.bestpl.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesuite.bestpl.api.IMessageService;
import com.jeesuite.bestpl.api.IPostService;
import com.jeesuite.bestpl.dto.Comment;
import com.jeesuite.bestpl.dto.Message;
import com.jeesuite.bestpl.dto.Page;
import com.jeesuite.bestpl.dto.PageMetaInfo;
import com.jeesuite.bestpl.dto.PageQueryParam;
import com.jeesuite.bestpl.dto.Post;
import com.jeesuite.passport.LoginContext;

@Controller
@RequestMapping("/ucenter")
public class UCenterController {
	
	private static final String PNG = "png";
	
	private final static int DEFAULT_PAGE_SIZE = 10;
	private static List<String> allow_upload_suffix = new ArrayList<>(Arrays.asList("png","jpg","jpeg"));
	
	private @Autowired IPostService postService;
	private @Autowired IMessageService messageService;
	
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(Model model){
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("ucenter"));
		
		return "ucenter/index";
	}
	
	@RequestMapping(value = "messages", method = RequestMethod.GET)
	public String message(Model model){
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("ucenter"));
		List<Message> messages = messageService.findUserNotReadMessage(LoginContext.getLoginSession().getUserId());
		model.addAttribute("messages", messages);
		return "ucenter/message";
	}
	
	@RequestMapping(value = "myposts", method = RequestMethod.POST)
	public @ResponseBody Page<Post> myPosts(@RequestParam(value="page",required=false) int pageNo){
		PageQueryParam param = new PageQueryParam(pageNo <= 0 ? 1 : pageNo,DEFAULT_PAGE_SIZE);
		param.getConditions().put("userId", LoginContext.getLoginSession().getUserId());
		return postService.pageQueryPost(param);
	}
	
	@RequestMapping(value = "mycomments", method = RequestMethod.POST)
	public @ResponseBody Page<Comment> myComments(@RequestParam(value="page",required=false) int pageNo){
		//TODO
		Page<Comment> page = new Page<>(1, 10, 0, new ArrayList<>());
		return page;
	}
	

}
