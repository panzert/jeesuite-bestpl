package com.jeesuite.bestpl.event;

import com.google.common.eventbus.Subscribe;
import com.jeesuite.bestpl.BestplConstants;
import com.jeesuite.bestpl.dto.Message;
import com.jeesuite.common.json.JsonUtils;
import com.jeesuite.kafka.spring.TopicProducerSpringProvider;

public class MessageSendEventListener {

	private TopicProducerSpringProvider topicProducerProvider; 
	private Message param;
	
	
	
	public MessageSendEventListener(TopicProducerSpringProvider topicProducerProvider) {
		this.topicProducerProvider = topicProducerProvider;
	}

	@Subscribe  
    public void listen(Message message) {  
        param = message;  
        //通过kafka发送
        topicProducerProvider.publishNoWrapperMessage(BestplConstants.MSG_SEND_TOPIC, JsonUtils.toJson(message));
    }

	public Message getParam() {
		return param;
	}

	public void setParam(Message param) {
		this.param = param;
	}

	
	
}
