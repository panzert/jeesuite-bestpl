package com.jeesuite.bestpl.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.bestpl.dao.entity.TagEntity;
import com.jeesuite.cache.CacheExpires;
import com.jeesuite.mybatis.plugin.cache.annotation.Cache;

import tk.mybatis.mapper.common.BaseMapper;

public interface TagEntityMapper extends BaseMapper<TagEntity> {
	
	@Cache(expire=CacheExpires.IN_5MINS)
	@Select("SELECT * FROM sc_tags  where name=#{name} limit 1")
	@ResultMap("BaseResultMap")
	TagEntity findByName(@Param("name") String name);
	
	@Cache(expire=CacheExpires.IN_1HOUR)
	@Select("SELECT * FROM sc_tags where refer_count>0  order by refer_count desc limit #{limit}")
	@ResultMap("BaseResultMap")
	List<TagEntity> findHotTags(int limit);
	
	int addTagRalate(@Param("postId") int postId,@Param("tagIds") int[] tagIds);
}