package com.jeesuite.bestpl.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.bestpl.dao.entity.MessageEntity;
import tk.mybatis.mapper.common.BaseMapper;

public interface MessageEntityMapper extends BaseMapper<MessageEntity> {
	
	@Select("SELECT * FROM sc_message  where to_uid=#{userId} where is_read=0 order by created_at desc")
	@ResultMap("BaseResultMap")
	List<MessageEntity> findNotReadMsgByUserId(int userId);
}