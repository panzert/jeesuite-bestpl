package com.jeesuite.bestpl.dao.entity;

import com.jeesuite.mybatis.core.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "sc_message")
public class MessageEntity extends BaseEntity {
    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 消息类型
     */
    @Column(name = "msg_type")
    private Integer msgType = 1;

    /**
     * 发送用户id
     */
    @Column(name = "from_uid")
    private Integer fromUid;

    @Column(name = "from_uname")
    private String fromUname;

    /**
     * 接受用户id
     */
    @Column(name = "to_uid")
    private Integer toUid;

    @Column(name = "to_uname")
    private String toUname;

    /**
     * 是否已读
     */
    @Column(name = "is_read")
    private Boolean isRead;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    private Date createdAt;

    /**
     * 内容
     */
    private String content;

    /**
     * 获取主键ID
     *
     * @return id - 主键ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键ID
     *
     * @param id 主键ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取消息类型
     *
     * @return msg_type - 消息类型
     */
    public Integer getMsgType() {
        return msgType;
    }

    /**
     * 设置消息类型
     *
     * @param msgType 消息类型
     */
    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    /**
     * 获取发送用户id
     *
     * @return from_uid - 发送用户id
     */
    public Integer getFromUid() {
        return fromUid;
    }

    /**
     * 设置发送用户id
     *
     * @param fromUid 发送用户id
     */
    public void setFromUid(Integer fromUid) {
        this.fromUid = fromUid;
    }

    /**
     * @return from_uname
     */
    public String getFromUname() {
        return fromUname;
    }

    /**
     * @param fromUname
     */
    public void setFromUname(String fromUname) {
        this.fromUname = fromUname;
    }

    /**
     * 获取接受用户id
     *
     * @return to_uid - 接受用户id
     */
    public Integer getToUid() {
        return toUid;
    }

    /**
     * 设置接受用户id
     *
     * @param toUid 接受用户id
     */
    public void setToUid(Integer toUid) {
        this.toUid = toUid;
    }

    /**
     * @return to_uname
     */
    public String getToUname() {
        return toUname;
    }

    /**
     * @param toUname
     */
    public void setToUname(String toUname) {
        this.toUname = toUname;
    }

    /**
     * 获取是否已读
     *
     * @return is_read - 是否已读
     */
    public Boolean getIsRead() {
        return isRead;
    }

    /**
     * 设置是否已读
     *
     * @param isRead 是否已读
     */
    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    /**
     * 获取创建时间
     *
     * @return created_at - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取内容
     *
     * @return content - 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }
}